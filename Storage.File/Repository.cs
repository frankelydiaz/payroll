﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Newtonsoft.Json;
using Payroll.Core.Entities;
using Payroll.Core.Repositories;

namespace Payroll.Storage.File
{
    public class Repository<T> : IRepository<T> where T : IEntity
    {
        protected IList<T> Data { get; set; }

        private  string StorageDirectory
        {
            get
            {
                return ConfigurationManager.AppSettings["StorageDirectory"]; 
            }
        }

        private string FilePath
        {
            get { return string.Format(@"{0}{1}.json", StorageDirectory, typeof(T).Name); }
        }

        private IList<T> DataSourceFile
        {
            get
            {
                if (!System.IO.File.Exists(FilePath))
                    return new List<T>();


                var dataFromFile = System.IO.File.ReadAllText(FilePath);

                return JsonConvert.DeserializeObject<List<T>>(dataFromFile);

            }
            set
            {
                if (!System.IO.Directory.Exists(StorageDirectory))
                    System.IO.Directory.CreateDirectory(StorageDirectory);

                var jsonData = JsonConvert.SerializeObject(value);
                System.IO.File.WriteAllText(FilePath,jsonData);

                
            }
        }


        public Repository()
        {
            Data = DataSourceFile;
        }

        public void Add(T entity)
        {
            Data.Add(entity);

            DataSourceFile = Data;
        }

        public IQueryable<T> GetQueryable()
        {
            return Data.AsQueryable();
        }

        public T FindById(int id)
        {
            return Data.FirstOrDefault(d => d.Id == id);
        }

        public void Update(T updatedEntity)
        {
            var dataWithoutUpdatedEntity = Data.Where(t => !t.Id.Equals(updatedEntity.Id)).ToList();

            dataWithoutUpdatedEntity.Add(updatedEntity);

            Data = dataWithoutUpdatedEntity;

            DataSourceFile = Data;
        }

        public void AddBulk(IList<T> newEntities)
        {
            foreach (var entity in newEntities)
            {
                Data.Add(entity);
            }

            DataSourceFile = Data;
        }

        public void Clear()
        {
            Data.Clear();

            DataSourceFile = Data;
        }
    }
}
