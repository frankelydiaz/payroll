using System.Collections.Generic;
using Newtonsoft.Json;

namespace Payroll.Core.Providers
{
    internal class RateResponse
    {
        [JsonProperty(PropertyName = "query")]
        public Query Query { get; set; }
    }

    internal class Rate
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Rate")]
        public string Value { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Ask { get; set; }
        public string Bid { get; set; }
    }

    internal class Results
    {
        [JsonProperty(PropertyName = "rate")]
        public List<Rate> Rates { get; set; }
    }

    internal class Query
    {
        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }

        [JsonProperty(PropertyName = "created")]
        public string Created { get; set; }

        [JsonProperty(PropertyName = "lang")]
        public string Lang { get; set; }

        [JsonProperty(PropertyName = "results")]
        public Results Results { get; set; }
    }
}