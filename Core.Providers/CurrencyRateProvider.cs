﻿using System.Collections.Generic;
using Payroll.Core.Entities;

namespace Payroll.Core.Providers
{
    public class CurrencyRateProvider : ICurrencyRateProvider
    {

        private readonly ICurrencyProvider _currencyProvider;

        public CurrencyRateProvider(ICurrencyProvider currencyProvider)
        {
            _currencyProvider = currencyProvider;
        }

        public IList<ICurrencyRate> GetRates(string baseCurrency, IList<string> currencies)
        {
            var currenciesPairs = CurrencyRateHelper.GetCurrenciesPairs(baseCurrency, currencies);

            var rateResponse = CurrencyRateHelper.GetRatesFromService(currenciesPairs);

            var currenciesRates = CurrencyRateHelper.TransformToCurrencyRates(baseCurrency, rateResponse);

            return currenciesRates;
        }

        public IList<ICurrencyRate> GetRates(string baseCurrency)
        {
            var currencies = _currencyProvider.GetCurrencies();

            return GetRates(baseCurrency, currencies);
        }
    }


}
