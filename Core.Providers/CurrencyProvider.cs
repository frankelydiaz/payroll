﻿using System;
using System.Collections.Generic;

namespace Payroll.Core.Providers
{
    public class CurrencyProvider : ICurrencyProvider
    {
        public IList<string> GetCurrencies()
        {
            return new List<string> { "USD", "DOP", "EUR" };
        }
    }
}