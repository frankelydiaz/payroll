﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using Payroll.Core.Entities;

namespace Payroll.Core.Providers
{
    public class CurrencyRateHelper
    {
        internal static IList<ICurrencyRate> TransformToCurrencyRates(string baseCurrency, RateResponse rateResponse)
        {
            return rateResponse.Query.Results.Rates.Select(rate => new CurrencyRate
            {
                BaseCurrency = baseCurrency, 
                Bid = decimal.Parse(rate.Bid), 
                Currency = rate.Id.Replace(baseCurrency, string.Empty)
            })
            .Where(c => !string.IsNullOrEmpty(c.Currency))
            .Cast<ICurrencyRate>().ToList();
        }

        internal static RateResponse GetRatesFromService(string currenciesPairs)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(@"http://query.yahooapis.com/v1/public/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response =
                     client.GetAsync(
                        string.Format(
                            @"yql?q=select * from yahoo.finance.xchange where pair in ({0})&env=store://datatables.org/alltableswithkeys&format=json",
                            currenciesPairs)).Result;

                response.EnsureSuccessStatusCode();

                var stringContent = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<RateResponse>(stringContent);


            }

            return null;
        }

        internal static string GetCurrenciesPairs(string baseCurrency, IList<string> currencies)
        {
            var currenciesWhere = new StringBuilder();

            for (var currencyIndex = 0; currencyIndex < currencies.Count; currencyIndex++)
            {

                currenciesWhere.Append(string.Format("'{0}{1}'", baseCurrency, currencies[currencyIndex]));

                if (currencyIndex + 1 < currencies.Count)
                    currenciesWhere.Append(",");
            }

            return currenciesWhere.ToString();
        }
    }
}