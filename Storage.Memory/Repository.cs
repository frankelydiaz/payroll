﻿using System;
using System.Collections;
using Payroll.Core.Repositories;
using System.Collections.Generic;
using System.Linq;
using Payroll.Core.Entities;

namespace Payroll.Storage.Memory
{
    public class Repository<T> : IRepository<T> where T : IEntity
    {
        protected IList<T> Data { get; set; }

        public Repository()
        {
            Data = new List<T>();
        }

        public void Add(T entity)
        {
            Data.Add(entity);
        }

        public IQueryable<T> GetQueryable()
        {
            return Data.AsQueryable();
        }

        public T FindById(int id)
        {
            return Data.FirstOrDefault(d => d.Id == id);
        }

        public void Update(T updatedEntity)
        {
            var dataWithoutUpdatedEntity = Data.Where(t => !t.Id.Equals(updatedEntity.Id)).ToList();

            dataWithoutUpdatedEntity.Add(updatedEntity);

            Data = dataWithoutUpdatedEntity;
        }

        public void AddBulk(IList<T> newEntities)
        {
            foreach (var entity in newEntities)
            {
                Data.Add(entity);
            }
        }

        public void Clear()
        {
            Data.Clear();
        }
    }
}