﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Client.Console
{
    public class ConsoleStyleUtils
    {
        private const int TableWidth = 80;

        public static void PrintLine()
        {
            System.Console.WriteLine(new string('-', TableWidth));
        }

        public static void PrintChar(char chartToPrint, int frequencies)
        {
        
        }


        public static void PrintRow(params object[] columns)
        {
            var width = (TableWidth - columns.Length) / columns.Length;
            var row = columns.Aggregate("|", (current, column) => current + (AlignCentre(column.ToString(), width) + "|"));

            System.Console.WriteLine(row);
        }

        public static string AlignCentre(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }
    }
}
