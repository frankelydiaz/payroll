﻿using System;
using Microsoft.Practices.Unity;
using Payroll.Core.Contexts;

namespace Payroll.Client.Console
{
    class Program
    {
        public Application Application { get; set; }


        private const string RegisterEmployeeOption = "1";
        private const string RegisterWorkedHoursOption = "2";
        private const string GeneratePaymentReportOption = "3";
        public const string FetchCurrencyRatesOption = "4";

        public Program(Application application)
        {
            Application = application;
        }


        static void Main(string[] args)
        {

            var program = IocContainer.Container.Resolve<Program>();

            var company = IocContainer.Container.Resolve<IFindFirstCompanyContext>().Exec();

            if (company == null)
                IocContainer.Container.Resolve<ICreateCompanyContext>().Exec(1, "Company X", "DOP", 1000);

            company = IocContainer.Container.Resolve<IFindFirstCompanyContext>().Exec();



            System.Console.WriteLine("Welcome to {0}", company.Name);


            while (true)
            {
                System.Console.WriteLine("");
                System.Console.WriteLine("Options:");
                System.Console.WriteLine("1:Register Employee");
                System.Console.WriteLine("2:Register Worked Hours");
                System.Console.WriteLine("3:Generate Payment Report");
                System.Console.WriteLine("4:Fetch Currency Rates");
                System.Console.WriteLine("Type exit to end");
                System.Console.WriteLine("");

                var key = System.Console.ReadLine();

                try
                {
                    switch (key)
                    {
                        case RegisterEmployeeOption:
                            {
                                program.Application.RegisterEmployee(company.Id);

                                break;
                            }
                        case RegisterWorkedHoursOption:
                            {
                                program.Application.RegisterWorkedHours(company.Id);
                                break;
                            }
                        case GeneratePaymentReportOption:
                            {
                                program.Application.ProcessPayment(company);
                                break;
                            }

                        case FetchCurrencyRatesOption:
                            {
                                program.Application.FetchCurrencies(company.Currency);
                                break;

                            }


                        case EntryUtils.ExitCommand:
                            {
                                return;
                            }
                        default:
                            {
                                System.Console.WriteLine();
                                System.Console.WriteLine("Invalid Option, Try Again");
                                System.Console.WriteLine("");
                                break;
                            }
                    }
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e.Message);
                }
            }
        }


    }
}
