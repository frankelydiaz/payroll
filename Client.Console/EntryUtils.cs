using System;
using Microsoft.Practices.Unity;
using Payroll.Core.Exceptions;
using Payroll.Core.Validators;

namespace Payroll.Client.Console
{
    public class EntryUtils
    {

        public const string ExitCommand = "exit";

        public static string ReadCurrency(string message, string defaultValue = null)
        {

            while (true)
            {
                var currency = ReadEntry(message, defaultValue);

                try
                {
                    IocContainer.Container.Resolve<ICurrencyValidator>().Validate(currency);

                    return currency;
                }
                catch (InvalidValueException e)
                {
                    System.Console.WriteLine(e.Message);
                }

            }
        }

        public static string ReadEntry(string message, string defaultValue = null)
        {
            var entry = string.Empty;

            while (entry == string.Empty)
            {
                try
                {
                    System.Console.WriteLine("");
                    System.Console.WriteLine(message);
                    entry = System.Console.ReadLine();

                    if (entry != null) entry = entry.Trim();

                    if (defaultValue != null && string.IsNullOrEmpty(entry))
                    {
                        System.Console.WriteLine(defaultValue);
                        return defaultValue;
                    }

                    if (entry == ExitCommand)
                        throw new Exception("Exit");



                }
                catch (InvalidValueException e)
                {
                    System.Console.WriteLine(e.Message);
                }

            }



            return entry;
        }

        public static int ReadNumberEntry(string message)
        {
            var entry = string.Empty;

            while (entry == string.Empty)
            {
                try
                {
                    System.Console.WriteLine("");
                    System.Console.WriteLine(message);

                    entry = System.Console.ReadLine();

                    if (entry == ExitCommand)
                        throw new Exception("Exit");

                    var number = 0;

                    if (!int.TryParse(entry, out number)) throw new InvalidValueException("Invalid number, Try Again");

                    if (number < 0)
                        throw new InvalidValueException("Number must be greater or equals than zero");

                    System.Console.WriteLine(entry);

                    return number;
                }
                catch (InvalidValueException e)
                {
                    entry = string.Empty;
                    System.Console.WriteLine(e.Message);
                }

            }

            return 0;
        }
    }
}