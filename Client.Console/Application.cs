using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using Payroll.Core.Contexts;
using Payroll.Core.Entities;
using Payroll.Core.Exceptions;
using Payroll.Core.Presenters;

namespace Payroll.Client.Console
{
    public class Application
    {

        private readonly IReportEmployeeWorkedHoursInWeekContext _employeeWorkedHoursInWeekContext;
        private readonly ICreateEmployeeContext _createEmployeeContext;
        private readonly IFindEmployeesWithWorkedHours _findEmployeesWithWorkedHours;
        private readonly IFindEmployeesWithoutWorkedHours _findEmployeesWithoutWorkedHours;
        private readonly IProcessWeeklyPaymentContext _processWeeklyPaymentContext;
        private readonly IFetchCurrencyRatesContext _fetchCurrencyRatesContext;

        public Application(ICreateEmployeeContext createEmployeeContext, IReportEmployeeWorkedHoursInWeekContext employeeWorkedHoursInWeekContext, IFindEmployeesWithoutWorkedHours findEmployeesWithoutWorkedHours, IFindEmployeesWithWorkedHours findEmployeesWithWorkedHours, IProcessWeeklyPaymentContext processWeeklyPaymentContext, IFetchCurrencyRatesContext fetchCurrencyRatesContext)
        {
            _createEmployeeContext = createEmployeeContext;
            _employeeWorkedHoursInWeekContext = employeeWorkedHoursInWeekContext;
            _findEmployeesWithoutWorkedHours = findEmployeesWithoutWorkedHours;
            _findEmployeesWithWorkedHours = findEmployeesWithWorkedHours;
            _processWeeklyPaymentContext = processWeeklyPaymentContext;
            _fetchCurrencyRatesContext = fetchCurrencyRatesContext;
        }

        public void FetchCurrencies(string baseCurrency)
        {
            foreach (var currency in _fetchCurrencyRatesContext.Exec(baseCurrency))
            {
                System.Console.WriteLine("{0} : {1}", currency.Currency, currency.Bid);
            }
        }

        public void RegisterEmployee(int companyId)
        {
            var employeeId = EntryUtils.ReadNumberEntry("Employee Id:");

            var employeeName = EntryUtils.ReadEntry("Employee Name:");

            var employeeCurrency = EntryUtils.ReadCurrency("Preferred Currency:");

            try
            {
                _createEmployeeContext.Exec(employeeId, employeeName, employeeCurrency, companyId);

                System.Console.WriteLine("Employee registered");
                System.Console.WriteLine("");
            }
            catch (BaseException e)
            {
                System.Console.WriteLine(e.Message);
            }
        }

        public void RegisterWorkedHours(int companyId)
        {

            var week = EntryUtils.ReadNumberEntry("Select Week Number:");

            var employeesWithWorkedHours = _findEmployeesWithWorkedHours.Exec(companyId, week);

            var employeesWithoutWorkedHours = _findEmployeesWithoutWorkedHours.Exec(companyId, week);

            var totalEmployees = employeesWithoutWorkedHours.Count + employeesWithWorkedHours.Count;

            if (totalEmployees == 0)
                throw new EntryNotFoundException(string.Format("There is not employees for week {0}", week));

            if (!employeesWithWorkedHours.Any())
                System.Console.WriteLine("{0} Employees with worked hours for week {1}", employeesWithWorkedHours.Count(), week);

            if (employeesWithoutWorkedHours.Count == 0)
                System.Console.WriteLine("All employees have been registered");

            employeesWithWorkedHours.ForEach(e =>
            {
                System.Console.WriteLine("Id: {0} - Employee: {1} - Worked Hours: {2} - Currency: {3}", e.Id, e.Name, e.WorkReports.FirstOrDefault().WorkedHours, e.PreferredCurrency);
            });


            employeesWithoutWorkedHours.ForEach(e =>
            {
                System.Console.WriteLine("Select Preferred Currency for Employee {0} Default [{1}]", e.Name, e.PreferredCurrency);


                var preferredCurrency = EntryUtils.ReadCurrency(string.Format("Preferred Currency [{0}]", e.PreferredCurrency), e.PreferredCurrency);
                var workedHours = EntryUtils.ReadNumberEntry(string.Format("Worked Hours", 0));

                _employeeWorkedHoursInWeekContext.Exec(workedHours, week, e.Id, preferredCurrency);

                System.Console.WriteLine("Worked Hours registered");

            });

        }



        public void ProcessPayment(Company company)
        {
            var week = EntryUtils.ReadNumberEntry("Select Week Number:");

            _fetchCurrencyRatesContext.Exec(company.Currency);

            var presenter = _processWeeklyPaymentContext.Exec(company.Id, week);

            System.Console.WriteLine("Weekly Payment {0}", company.Name);
            System.Console.WriteLine("{0:yyyy-MM-dd HH:mm:ss} Total: {1}{2:C}", presenter.Date, presenter.BaseCurrency, presenter.Total);
            ConsoleStyleUtils.PrintLine();
            System.Console.WriteLine("Rates: Base {0}", presenter.BaseCurrency);

            foreach (var currencyRate in presenter.CurrencyRates)
            {
                System.Console.WriteLine("To {0}: {1:N4}", currencyRate.Currency, currencyRate.Bid);
            }

            ConsoleStyleUtils.PrintLine();

            ConsoleStyleUtils.PrintRow("Entry", "Emp ID", "Fullname", "Currency", "Amount", "Base Amount");


            foreach (var entry in presenter.Rows)
            {
                ConsoleStyleUtils.PrintRow(entry.Entry, entry.EmployeeId, entry.EmployeeName, entry.Currency, entry.Amount, entry.BaseAmount);
            }
            ConsoleStyleUtils.PrintLine();

            System.Console.WriteLine("{0:yyyy-MM-dd HH:mm:ss} Total: {1}{2:C}", presenter.Date, presenter.BaseCurrency, presenter.Total);

            ConsoleStyleUtils.PrintLine();
            foreach (var totalPaymentByCurrency in presenter.TotalPaymentByCurrencies)
            {
                System.Console.WriteLine("Total {0}: {1:C}", totalPaymentByCurrency.Currency, totalPaymentByCurrency.Total);
            }
            ConsoleStyleUtils.PrintLine();

            StoreReport(presenter);
        }

        private static void StoreReport(IWeeklyPaymentPresenter report)
        {
            var json = JsonConvert.SerializeObject(report);
            var storageDirectory = ConfigurationManager.AppSettings["StorageDirectory"];

            System.IO.File.WriteAllText(string.Format(@"{0}Week-{1}-Report.json",storageDirectory, report.Week), json);
        }
    }
}