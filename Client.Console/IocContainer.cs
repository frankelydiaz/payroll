﻿using Microsoft.Practices.Unity;
using Payroll.Core.Contexts;
using Payroll.Core.Providers;
using Payroll.Core.Repositories;
using Payroll.Core.Validators;
using Payroll.Storage.File;

namespace Payroll.Client.Console
{
    class IocContainer
    {
        private static IUnityContainer _container;

        public static IUnityContainer Container
        {
            get { return _container ?? (_container = Register()); }
            set { _container = value; }
        }

        private static IUnityContainer Register()
        {
            Container = new UnityContainer();

            //Storage
            Container.RegisterType(typeof(IRepository<>), typeof(Repository<>), new ContainerControlledLifetimeManager());

            //Validators
            Container.RegisterType<ICurrencyValidator, CurrencyValidator>();

            //Providers
            Container.RegisterType<ICurrencyRateProvider, CurrencyRateProvider>();
            Container.RegisterType<ICurrencyProvider, CurrencyProvider>();

            //Contexts
            Container.RegisterType<ICreateCompanyContext, CreateCompanyContext>();
            Container.RegisterType<ICreateEmployeeContext, CreateEmployeeContext>();
            Container.RegisterType<IReportEmployeeWorkedHoursInWeekContext, ReportEmployeeWorkedHoursInWeekContext>();
            Container.RegisterType<IRegisterCurrencyRatesContext, RegisterCurrencyRatesContext>();
            Container.RegisterType<IProcessWeeklyPaymentContext, ProcessWeeklyPaymentContext>();
            Container.RegisterType<IFetchCurrencyRatesContext, FetchCurrencyRatesContext>();
            Container.RegisterType<IFindFirstCompanyContext, FindFirstCompanyContext>();
            Container.RegisterType<IFindEmployeesWithWorkedHours, FindEmployeesWithWorkedHours>();
            Container.RegisterType<IFindEmployeesWithoutWorkedHours, FindEmployeesWithoutWorkedHours>();

            //Application
            Container.RegisterType<Application, Application>(new ContainerControlledLifetimeManager());

            return Container;
        }
    }
}