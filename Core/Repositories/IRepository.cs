﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Payroll.Core.Entities;

namespace Payroll.Core.Repositories
{
    public interface IRepository<T> where T : IEntity
    {
        void Add(T newEntity);
        IQueryable<T> GetQueryable();
        T FindById(int id);
        void Update(T updatedEntity);
        void AddBulk(IList<T> newEntities);
        void Clear();
    }


}
