using System;
using System.Collections.Generic;
using System.Linq;
using Payroll.Core.Entities;

namespace Payroll.Core.Presenters
{
    public class WeeklyPaymentPresenter : IWeeklyPaymentPresenter
    {
        private readonly IList<Payment> _payments;

        public int Week { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public decimal Total
        {
            get { return _payments.Sum(x => x.BaseAmount); }
        }

        public string BaseCurrency { get; set; }

        public DateTimeOffset Date { get; set; }

        public IList<ICurrencyRate> CurrencyRates { get; set; }

        public IList<IWeeklyPaymentPresenterRow> Rows
        {
            get
            {
                return _payments.Select(p => new WeeklyPaymentPresenterRow
                {
                    /*Amount = decimal.Round(p.Amount,2) ,
                    BaseAmount = Decimal.Round(p.BaseAmount,2),*/
                    Amount = p.Amount,
                    BaseAmount = p.BaseAmount,
                    Currency = p.Currency,
                    EmployeeId = p.EmployeeId,
                    EmployeeName = p.EmployeeName,
                    Entry = p.Id    
                }).ToList<IWeeklyPaymentPresenterRow>();
            }
        }

        public IList<ITotalPaymentByCurrency> TotalPaymentByCurrencies
        {
            get
            {
                return _payments
                    .GroupBy(g => g.Currency)
                    .Select(p => new TotalPaymentByCurrency
                    {
                        Currency = p.Key,
                        Total = p.Sum(x => x.Amount)
                    })
                    .ToList<ITotalPaymentByCurrency>();
            }
        }


        public WeeklyPaymentPresenter(Company company, IList<Payment> payments, IList<ICurrencyRate> currencyRates, int week)
        {
            _payments = payments;
            Date = DateTimeOffset.UtcNow;
            CurrencyRates = currencyRates;
            BaseCurrency = company.Currency;
            CompanyName = company.Name;
            CompanyId = company.Id;
            Week = week;
        }


    }
}