﻿using System;
using System.Collections.Generic;
using Payroll.Core.Entities;

namespace Payroll.Core.Presenters
{
    public interface IWeeklyPaymentPresenter
    {
        int Week { get; }

        int CompanyId { get; }

        string CompanyName { get; }

        decimal Total { get; }

        string BaseCurrency { get; set; }

        DateTimeOffset Date { get; }

        IList<ICurrencyRate> CurrencyRates { get; }
        IList<IWeeklyPaymentPresenterRow> Rows { get; }
        IList<ITotalPaymentByCurrency> TotalPaymentByCurrencies { get; }
    }

    public class TotalPaymentByCurrency : ITotalPaymentByCurrency
    {
        public string Currency { get; set; }

        public decimal Total { get; set; }
    }

    public interface ITotalPaymentByCurrency
    {
        string Currency { get; }
        decimal Total { get; }


    }

    public interface IWeeklyPaymentPresenterRow
    {
        int Entry { get; }
        int EmployeeId { get; }
        string EmployeeName { get; set; }
        string Currency { get; set; }
        decimal Amount { get; }
        decimal BaseAmount { get; }
    }

    public class WeeklyPaymentPresenterRow : IWeeklyPaymentPresenterRow
    {
        public int Entry { get; set; }

        public int EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public string Currency { get; set; }

        public decimal Amount { get; set; }

        public decimal BaseAmount { get; set; }
    }
}