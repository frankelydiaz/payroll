namespace Payroll.Core.Validators
{
    public interface ICurrencyValidator
    {
        void Validate(string currency);
    }
}