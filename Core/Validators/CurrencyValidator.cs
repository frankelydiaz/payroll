﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Payroll.Core.Exceptions;
using Payroll.Core.Providers;

namespace Payroll.Core.Validators
{
    public class CurrencyValidator : ICurrencyValidator
    {
        private readonly ICurrencyProvider _currencyProvider;

        public CurrencyValidator(ICurrencyProvider currencyProvider)
        {
            _currencyProvider = currencyProvider;
        }

        public void Validate(string currency)
        {
            var allowedCurrencies = _currencyProvider.GetCurrencies();

            if (allowedCurrencies.Contains(currency))
                return;

            throw new InvalidValueException(string.Format("Invalid currency {0} try {1} instead", currency, string.Join(", ", allowedCurrencies)));
        }

    }
}
