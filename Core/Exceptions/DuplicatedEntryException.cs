﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Core.Exceptions
{
    public class DuplicatedEntryException : BaseException
    {


        public DuplicatedEntryException(string message) : base(message)
        {

        }
    }
}
