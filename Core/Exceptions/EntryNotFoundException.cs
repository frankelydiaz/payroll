﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Core.Exceptions
{
    public class EntryNotFoundException : BaseException
    {
        private string p;

        public EntryNotFoundException(string message) : base(message)
        {
        }

  
    }
}
