﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Core.Exceptions
{
    public class BaseException : Exception
    {
        protected BaseException(string message) : base(message)
        {
          
        }
    }
}
