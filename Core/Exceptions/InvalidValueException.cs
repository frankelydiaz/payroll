﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Core.Exceptions
{
    public class InvalidValueException : BaseException
    {
        public InvalidValueException(string message) : base(message)
        {
        }
    }
}
