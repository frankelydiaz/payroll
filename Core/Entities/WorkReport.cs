namespace Payroll.Core.Entities
{
    public class WorkReport : BaseEntity
    {
        public string Currency { get; set; }
        public int Week { get; set; }
        public int WorkedHours { get; set; }
    }
}