﻿namespace Payroll.Core.Entities
{
    public interface IEntity 
    {
        int Id { get; set; }
    }

}
