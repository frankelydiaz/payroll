﻿namespace Payroll.Core.Entities
{
    public class Payment : BaseEntity
    {
        public string EmployeeName { get; set; }
        public int EmployeeId { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }

        public decimal Rate { get; set; }

        public decimal BaseAmount { get; set; }
        public string BaseCurrency { get; set; }
        public int Week { get; set; }
        public int CompanyId { get; set; }
    }
}