namespace Payroll.Core.Entities
{
    public interface ICurrencyRate
    {
        string Currency { get; }
        decimal Bid { get; }
    }
}