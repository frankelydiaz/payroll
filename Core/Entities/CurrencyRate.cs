﻿namespace Payroll.Core.Entities
{
    public class CurrencyRate : BaseEntity, ICurrencyRate
    {
        public string BaseCurrency { get; set; }
        public string Currency { get; set; }

        public decimal Bid { get; set; }
    }
}