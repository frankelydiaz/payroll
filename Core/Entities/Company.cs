﻿namespace Payroll.Core.Entities
{
    public class Company : BaseEntity
    {
        public string Name { get; set; }

        public string Currency { get; set; }

        public decimal FixedPrice { get; set; }
    }
}
