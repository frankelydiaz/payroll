using System.Collections;
using System.Collections.Generic;
using Payroll.Core.Contexts;

namespace Payroll.Core.Entities
{
    public class Employee : BaseEntity
    {
        public Employee()
        {
            WorkReports = new List<WorkReport>();
        }

        public string Name { get; set; }
        public string PreferredCurrency { get; set; }

        public IList<WorkReport> WorkReports { get; set; }
        public int CompanyId { get; set; }
    }
}