﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Payroll.Core.Entities;

namespace Payroll.Core.Contexts
{
    public interface IFindEmployeesWithWorkedHours
    {
        IList<Employee> Exec(int companyId, int week);
    }
}
