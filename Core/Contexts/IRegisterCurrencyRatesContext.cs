using System.Collections.Generic;
using Payroll.Core.Entities;

namespace Payroll.Core.Contexts
{
    public interface IRegisterCurrencyRatesContext
    {
        void Exec(IList<CurrencyRate> currencyRates);
    }
}