using System.Collections.Generic;
using System.Linq;
using Payroll.Core.Entities;
using Payroll.Core.Repositories;

namespace Payroll.Core.Contexts
{
    public class FindEmployeesWithoutWorkedHours : IFindEmployeesWithoutWorkedHours
    {
        private readonly IRepository<Employee> _employeeRepository;

        public FindEmployeesWithoutWorkedHours(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public IList<Employee> Exec(int companyId, int week)
        {
            var employeesWithoutWorkedHours = _employeeRepository.GetQueryable()
                .Where(e => e.CompanyId == companyId)
                .Where(e => e.WorkReports.All(w => w.Week != week)).ToList();

            return employeesWithoutWorkedHours;
        }
    }
}