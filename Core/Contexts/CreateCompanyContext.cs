﻿using Payroll.Core.Entities;
using Payroll.Core.Repositories;
using Payroll.Core.Validators;

namespace Payroll.Core.Contexts
{
    public class CreateCompanyContext : ICreateCompanyContext
    {
        private readonly IRepository<Company> _companyRepository;
        private readonly ICurrencyValidator _currencyValidator;

        public CreateCompanyContext(IRepository<Company> companyRepository, ICurrencyValidator currencyValidator)
        {
            _companyRepository = companyRepository;
            _currencyValidator = currencyValidator;
        }


        public void Exec(int id, string name, string currency, decimal fixedPrice)
        {

            _currencyValidator.Validate(currency);

            _companyRepository.Add(new Company
            {
                Id = id,
                Name = name,
                Currency = currency,
                FixedPrice = fixedPrice
            });
        }
    }
}
