using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using Payroll.Core.Entities;
using Payroll.Core.Exceptions;
using Payroll.Core.Presenters;
using Payroll.Core.Repositories;

namespace Payroll.Core.Contexts
{
    public class ProcessWeeklyPaymentContext : IProcessWeeklyPaymentContext
    {
        private readonly IRepository<CurrencyRate> _currencyRateRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Payment> _paymentRepository;
        private readonly IRepository<Company> _companyRepository;

        private IList<ICurrencyRate> _currencyRates;

        public ProcessWeeklyPaymentContext(IRepository<CurrencyRate> currencyRateRepository, IRepository<Employee> employeeRepository, IRepository<Payment> paymentRepository, IRepository<Company> companyRepository)
        {
            _currencyRateRepository = currencyRateRepository;
            _employeeRepository = employeeRepository;
            _paymentRepository = paymentRepository;
            _companyRepository = companyRepository;
        }


        public IWeeklyPaymentPresenter Exec(int companyId, int week)
        {
            var company = _companyRepository.FindById(companyId);

            ValidateWeek(companyId,week);

            _currencyRates = _currencyRateRepository.GetQueryable()
                .Where(cr => cr.BaseCurrency == company.Currency).ToList<ICurrencyRate>();

            var employeesWithWorkReport = _employeeRepository.GetQueryable()
                .Where(e => e.CompanyId == companyId)
                .Where(e => e.WorkReports.Any(w => w.Week == week));

            if (!employeesWithWorkReport.Any())
                throw new EntryNotFoundException(string.Format("No worked hours found for week {0}",week));

            var payments = GeneratePayments(employeesWithWorkReport,company.Id, company.Currency, company.FixedPrice, week);
            _paymentRepository.AddBulk(payments);


            return new WeeklyPaymentPresenter(company,payments,_currencyRates, week);


        }

        private void ValidateWeek(int companyId, int week)
        {
            var query = _paymentRepository.GetQueryable().Where(p => p.CompanyId == companyId);

            if (!query.Any())
                return;

            var lastWeekNumber = query.Max(p => p.Week);

            if (lastWeekNumber >= week)
                throw new InvalidDataException(string.Format("{0} week report has been already generated", lastWeekNumber));
        }

        private IList<Payment> GeneratePayments(IEnumerable<Employee> employees, int companyId, string baseCurrency, decimal fixedPrice, int week)
        {

            var payments = new List<Payment>();
            var paymentId = 1;

            foreach (var employee in employees)
            {


                var workReport = employee.WorkReports.FirstOrDefault(w => w.Week == week);

                if (workReport == null)
                    throw new RequiredException(string.Format("Employee {0} must have a work report", employee.Name));

                var rate = CalculateRate(baseCurrency, workReport.Currency);
                var amount = CalculateAmount(workReport.WorkedHours, fixedPrice,rate);
                var baseAmount = CalculateBaseAmount(workReport.WorkedHours, fixedPrice);

                var payment = new Payment
                {
                    BaseCurrency = baseCurrency,
                    Currency = workReport.Currency,
                    Amount = amount,
                    BaseAmount = baseAmount,
                    EmployeeId = employee.Id,
                    Id = paymentId++,
                    Rate = rate,
                    EmployeeName = employee.Name,
                    Week = week,
                    CompanyId = companyId

                };

                payments.Add(payment);
            }

            return payments;

        }

        private decimal CalculateBaseAmount(int workedHours, decimal fixedPrice)
        {
            return workedHours * fixedPrice;
        }

        private decimal CalculateRate(string baseCurrency, string currency)
        {
            const decimal defaultRate = 1;

            if (baseCurrency == currency)
                return defaultRate;

            var currencyRate = _currencyRates.FirstOrDefault(c => c.Currency == currency);

            if (currencyRate != null)
                return currencyRate.Bid;

            return 0;
        }

        private decimal CalculateAmount(int workedHours, decimal fixedPrice, decimal currencyRate)
        {
            return workedHours * fixedPrice * currencyRate;
        }



    }
}