﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Payroll.Core.Entities;

namespace Payroll.Core.Contexts
{
    public interface IFetchCurrencyRatesContext
    {

        IList<ICurrencyRate> Exec(string baseCurrency);

        IList<ICurrencyRate> Exec(string baseCurrency, IList<string> currencies);

    }
}
