﻿using Payroll.Core.Presenters;

namespace Payroll.Core.Contexts
{
    public interface IProcessWeeklyPaymentContext
    {
        IWeeklyPaymentPresenter Exec(int companyId, int week);
    }


}