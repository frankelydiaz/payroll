﻿using System.Collections.Generic;
using Payroll.Core.Entities;

namespace Payroll.Core.Contexts
{
    public interface IFindEmployeesWithoutWorkedHours
    {
        IList<Employee> Exec(int companyId, int week);
    }
}