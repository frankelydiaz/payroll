namespace Payroll.Core.Contexts
{
    public interface IReportEmployeeWorkedHoursInWeekContext
    {
        void Exec(int hours, int week, int employeeId, string preferredCurrency);
    }
}