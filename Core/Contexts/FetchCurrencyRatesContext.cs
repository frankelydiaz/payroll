﻿using System.Collections.Generic;
using System.Linq;
using Payroll.Core.Entities;
using Payroll.Core.Providers;
using Payroll.Core.Repositories;

namespace Payroll.Core.Contexts
{
    public class FetchCurrencyRatesContext : IFetchCurrencyRatesContext
    {
        private readonly ICurrencyRateProvider _currencyRateProvider;
        private readonly IRepository<CurrencyRate> _currencyRateRepository;

        public FetchCurrencyRatesContext(ICurrencyRateProvider currencyRateProvider, IRepository<CurrencyRate> currencyRateRepository)
        {
            _currencyRateProvider = currencyRateProvider;
            _currencyRateRepository = currencyRateRepository;
        }

        public IList<ICurrencyRate> Exec(string baseCurrency)
        {
            var id = 1;

            var currencyRates = _currencyRateProvider
                .GetRates(baseCurrency)
                .Select(s => new CurrencyRate
                {
                    BaseCurrency = baseCurrency,
                    Bid = s.Bid,
                    Currency = s.Currency,
                    Id = id++
                }).ToList();

            _currencyRateRepository.Clear();
            _currencyRateRepository.AddBulk(currencyRates);

            return currencyRates.ToList<ICurrencyRate>();

        }

        public IList<ICurrencyRate> Exec(string baseCurrency, IList<string> currencies)
        {
            var id = 1;

            var currencyRates = _currencyRateProvider
                .GetRates(baseCurrency, currencies)
                .Select(s => new CurrencyRate
                {
                    BaseCurrency = baseCurrency,
                    Bid = s.Bid,
                    Currency = s.Currency,
                    Id = id++
                }).ToList();


            _currencyRateRepository.Clear();
            _currencyRateRepository.AddBulk(currencyRates);

            return currencyRates.ToList<ICurrencyRate>();
        }
    }
}