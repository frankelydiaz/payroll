namespace Payroll.Core.Contexts
{
    public interface ICreateEmployeeContext
    {
        void Exec(int id, string name, string preferredCurrency, int companyId);
    }
}