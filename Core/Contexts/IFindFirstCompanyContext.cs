﻿using Payroll.Core.Entities;

namespace Payroll.Core.Contexts
{
    public interface IFindFirstCompanyContext
    {
        Company Exec();
    }
}