using System;
using Payroll.Core.Entities;
using Payroll.Core.Exceptions;
using Payroll.Core.Repositories;
using Payroll.Core.Validators;

namespace Payroll.Core.Contexts
{
    public class CreateEmployeeContext : ICreateEmployeeContext
    {
        private readonly IRepository<Employee> _employeRepository;
        private readonly ICurrencyValidator _currencyValidator;

        public CreateEmployeeContext(IRepository<Employee> employeRepository, ICurrencyValidator currencyValidator)
        {
            _employeRepository = employeRepository;
            _currencyValidator = currencyValidator;
        }

        public void Exec(int id, string name, string preferredCurrency, int companyId)
        {
            _currencyValidator.Validate(preferredCurrency);

            var employee = _employeRepository.FindById(id);

            if (employee != null)
                throw new DuplicatedEntryException(string.Format("The employee {0} with the id {1} already exists", name, id));
            

            _employeRepository.Add(new Employee
            {
                Id = id,
                Name = name,
                PreferredCurrency = preferredCurrency,
                CompanyId = companyId
            });
        }
    }
}