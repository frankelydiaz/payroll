﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Payroll.Core.Entities;
using Payroll.Core.Repositories;

namespace Payroll.Core.Contexts
{
    public class FindFirstCompanyContext : IFindFirstCompanyContext
    {
        private readonly IRepository<Company> _companyRepository;

        public FindFirstCompanyContext(IRepository<Company> companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public Company Exec()
        {
           return _companyRepository.GetQueryable().FirstOrDefault();
        }
    }
}
