﻿namespace Payroll.Core.Contexts
{
    public interface ICreateCompanyContext
    {
        void Exec(int id,string name, string currency, decimal fixedPrice);
    }
}
