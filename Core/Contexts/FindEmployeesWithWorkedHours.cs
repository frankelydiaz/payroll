using System.Collections.Generic;
using System.Linq;
using Payroll.Core.Entities;
using Payroll.Core.Repositories;

namespace Payroll.Core.Contexts
{
    public class FindEmployeesWithWorkedHours : IFindEmployeesWithWorkedHours
    {
        private readonly IRepository<Employee> _employeeRepository;

        public FindEmployeesWithWorkedHours(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public IList<Employee> Exec(int companyId, int week)
        {
            var employeesWithWorkedHours = _employeeRepository.GetQueryable()
                .Where(e => e.CompanyId == companyId)
                .Where(e => e.WorkReports.Any(w => w.Week == week)).ToList();

            return employeesWithWorkedHours;
        }
    }
}