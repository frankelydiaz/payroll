using System;
using Payroll.Core.Entities;
using Payroll.Core.Exceptions;
using Payroll.Core.Repositories;
using Payroll.Core.Validators;

namespace Payroll.Core.Contexts
{
    public class ReportEmployeeWorkedHoursInWeekContext : IReportEmployeeWorkedHoursInWeekContext
    {

        private readonly IRepository<Employee> _employeeRepository;
        private readonly ICurrencyValidator _currencyValidator;

        public ReportEmployeeWorkedHoursInWeekContext(IRepository<Employee> employeeRepository, ICurrencyValidator currencyValidator)
        {
            _employeeRepository = employeeRepository;
            _currencyValidator = currencyValidator;
        }

        public void Exec(int hours, int week, int employeeId, string preferredCurrency)
        {

            _currencyValidator.Validate(preferredCurrency);

            if (hours < 0)
                throw new RequiredException("Hours must be greater or equals to zero");

            var employee = _employeeRepository.FindById(employeeId);

            if (employee == null)
                throw new EntryNotFoundException(string.Format("Employee {0} not found",employeeId));

            employee.PreferredCurrency = preferredCurrency;

            employee.WorkReports.Add(new WorkReport
            {
                Week = week,
                WorkedHours = hours,
                Currency = employee.PreferredCurrency
            });


            _employeeRepository.Update(employee);

        }
    }
}