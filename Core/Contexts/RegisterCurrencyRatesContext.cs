using System.Collections.Generic;
using Payroll.Core.Entities;
using Payroll.Core.Repositories;

namespace Payroll.Core.Contexts
{
    public class RegisterCurrencyRatesContext : IRegisterCurrencyRatesContext
    {
        private readonly IRepository<CurrencyRate> _currencyRateRepository;

        public RegisterCurrencyRatesContext(IRepository<CurrencyRate> currencyRateRepository)
        {
            _currencyRateRepository = currencyRateRepository;
        }

        public void Exec(IList<CurrencyRate> currencyRates)
        {
            _currencyRateRepository.AddBulk(currencyRates);
        }
    }
}