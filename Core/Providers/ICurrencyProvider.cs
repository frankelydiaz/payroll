﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Core.Providers
{
    public interface ICurrencyProvider
    {
       IList<string> GetCurrencies();
    }
}
