﻿using System.Collections.Generic;
using Payroll.Core.Entities;

namespace Payroll.Core.Providers
{
    public interface ICurrencyRateProvider
    {
        IList<ICurrencyRate> GetRates(string baseCurrency, IList<string> currencies);
        IList<ICurrencyRate> GetRates(string baseCurrency);
    }
}