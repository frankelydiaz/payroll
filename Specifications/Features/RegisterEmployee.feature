﻿Feature: RegisterEmployee
	In order to generate payment report
	As an user
	I want to register all my employees with an unique id

Scenario: Register Employee
	Given I have registered "Frankely Diaz" with id "1" and preferredCurrency "DOP" and companyId 1
	Then I must not be able to register "Pedro" with id 1 and preferredCurrency "USD" and companyId 1
