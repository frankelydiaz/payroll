﻿Feature: Fetch Currency Conversion Rates
	In order to convert my base currency to another
	As an user
	I want to be fetch new rates for my base currency


Scenario: Fetch currency rates
	Given I have the base currency as "DOP"
	And I have these currencies:
	| Currency |
	| EUR      |
	| USD      |
	Then I fetch these currencies 
	And the total of currencies fetched should be 2
	And The currencies fetched should be:
	| Currency |
	| EUR      |
	| USD      |
