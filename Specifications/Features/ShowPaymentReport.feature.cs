﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.34209
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Payroll.Specifications.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute()]
    public partial class ShowPaymentReportFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "ShowPaymentReport.feature"
#line hidden
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassInitializeAttribute()]
        public static void FeatureSetup(Microsoft.VisualStudio.TestTools.UnitTesting.TestContext testContext)
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "ShowPaymentReport", "As an employeer\r\nI want to see a report with all the payment details\r\nIn order to" +
                    " see what i have to pay to each of my employees", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassCleanupAttribute()]
        public static void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestInitializeAttribute()]
        public virtual void TestInitialize()
        {
            if (((TechTalk.SpecFlow.FeatureContext.Current != null) 
                        && (TechTalk.SpecFlow.FeatureContext.Current.FeatureInfo.Title != "ShowPaymentReport")))
            {
                Payroll.Specifications.Features.ShowPaymentReportFeature.FeatureSetup(null);
            }
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCleanupAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 7
#line 8
 testRunner.Given("The company with id 1 and name \"Novosit\" with its base currency in \"DOP\" with the" +
                    " hours paid at a fixed price of \"1000.00\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "Currency",
                        "Value"});
            table1.AddRow(new string[] {
                        "USD",
                        "0.0333"});
            table1.AddRow(new string[] {
                        "EUR",
                        "0.0196"});
#line 9
 testRunner.Given("The currency conversion rates for \"DOP\" are", ((string)(null)), table1, "Given ");
#line 13
 testRunner.Given("the employee with id 1 with fullname \"Frankely Diaz\" with The preferred currency " +
                    "\"DOP\" in the company 1 is registered", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 14
 testRunner.Given("the employee with id 2 with fullname \"Martin Perez Fowler\" with The preferred cur" +
                    "rency \"USD\" in the company 1 is registered", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("View Weekly Payment")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "ShowPaymentReport")]
        public virtual void ViewWeeklyPayment()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("View Weekly Payment", ((string[])(null)));
#line 15
this.ScenarioSetup(scenarioInfo);
#line 7
this.FeatureBackground();
#line 16
 testRunner.When("Employee 1 Reported 5 worked hours in the week number 2 with currency \"DOP\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 17
 testRunner.And("Employee 2 Reported 9 worked hours in the week number 2 with currency \"USD\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                        "Entry",
                        "EmployeeId",
                        "EmployeeName",
                        "Currency",
                        "Amount",
                        "BaseAmount"});
            table2.AddRow(new string[] {
                        "1",
                        "1",
                        "Frankely Diaz",
                        "DOP",
                        "5000.00",
                        "5000.00"});
            table2.AddRow(new string[] {
                        "2",
                        "2",
                        "Martin Perez Fowler",
                        "USD",
                        "299.70",
                        "9000.00"});
#line 18
 testRunner.Then("I process the payment report for the company 1 for the week number 2 and I should" +
                    " see the following report:", ((string)(null)), table2, "Then ");
#line hidden
            TechTalk.SpecFlow.Table table3 = new TechTalk.SpecFlow.Table(new string[] {
                        "Currency",
                        "Total"});
            table3.AddRow(new string[] {
                        "DOP",
                        "5000.00"});
            table3.AddRow(new string[] {
                        "USD",
                        "299.70"});
#line 22
 testRunner.And("The total by currency should be:", ((string)(null)), table3, "And ");
#line 26
 testRunner.And("the total should be 14000", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
