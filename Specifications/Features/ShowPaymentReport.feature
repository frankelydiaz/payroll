﻿Feature: ShowPaymentReport
	As an employeer
	I want to see a report with all the payment details
	In order to see what i have to pay to each of my employees


Background: 
	Given The company with id 1 and name "Novosit" with its base currency in "DOP" with the hours paid at a fixed price of "1000.00" 
	Given The currency conversion rates for "DOP" are 
	| Currency | Value		|
	| USD      | 0.0333		|
	| EUR      | 0.0196		|
	Given the employee with id 1 with fullname "Frankely Diaz" with The preferred currency "DOP" in the company 1 is registered
	Given the employee with id 2 with fullname "Martin Perez Fowler" with The preferred currency "USD" in the company 1 is registered
Scenario: View Weekly Payment
	When Employee 1 Reported 5 worked hours in the week number 2 with currency "DOP"
	And Employee 2 Reported 9 worked hours in the week number 2 with currency "USD"
	Then I process the payment report for the company 1 for the week number 2 and I should see the following report:
	 | Entry | EmployeeId | EmployeeName            | Currency | Amount		| BaseAmount |
	 | 1     | 1          | Frankely Diaz			| DOP      | 5000.00	| 5000.00   |
	 | 2     | 2          | Martin Perez Fowler		| USD      | 299.70		| 9000.00   |
	And The total by currency should be:
	| Currency | Total   |
	| DOP      | 5000.00 |
	| USD      | 299.70  |
	And the total should be 14000