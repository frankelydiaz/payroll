﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Payroll.Core.Contexts;
using Payroll.Core.Entities;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Payroll.Specifications.Steps
{
    [Binding]
    public class FetchCurrencyConversionRatesSteps : BaseSteps
    {
        private string _baseCurrency;
        private IList<string> _currencies;
        private IList<ICurrencyRate> _currencyRates;



        [Given(@"I have the base currency as ""(.*)""")]
        public void GivenIHaveTheBaseCurrencyAs(string baseCurrency)
        {
            _baseCurrency = baseCurrency;
        }

        [Given(@"I have these currencies:")]
        public void GivenIHaveTheseCurrencies(Table table)
        {
            _currencies = table.Rows.Select(r => r[0].ToString()).ToList();
        }

        [Then(@"I fetch these currencies")]
        public void ThenIFetchTheseCurrencies()
        {
            _currencyRates = Container.Resolve<IFetchCurrencyRatesContext>().Exec(_baseCurrency, _currencies);
        }

        [Then(@"the total of currencies fetched should be (.*)")]
        public void ThenTheTotalOfCurrenciesFetchedShouldBe(int total)
        {
            Assert.AreEqual(total, _currencyRates.Count);
        }

        [Then(@"The currencies fetched should be:")]
        public void ThenTheCurrenciesFetchedShouldBe(Table table)
        {
            var currencies = _currencyRates.Select(r => new
            {
                r.Currency
            }).ToList();

            table.CompareToSet(currencies);
        }

    }
}
