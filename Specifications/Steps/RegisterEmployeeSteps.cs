﻿using System;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Payroll.Core.Contexts;
using Payroll.Core.Exceptions;
using TechTalk.SpecFlow;

namespace Payroll.Specifications.Steps
{
    [Binding]
    public class RegisterEmployeeSteps : BaseSteps
    {

        [Given(@"I have registered ""(.*)"" with id ""(.*)"" and preferredCurrency ""(.*)"" and companyId (.*)")]
        public void RegisterEmployee(string employeeName, int employeeId, string preferredCurrency, int companyId)
        {
            Container.Resolve<ICreateEmployeeContext>().Exec(employeeId, employeeName, preferredCurrency, companyId);
        }

        [Then(@"I must not be able to register ""(.*)"" with id (.*) and preferredCurrency ""(.*)"" and companyId (.*)")]
        public void RegisterEmployeeWithExistingId(string employeeName, int employeeId, string preferredCurrency, int companyId)
        {
            try
            {
                Container.Resolve<ICreateEmployeeContext>().Exec(employeeId, employeeName, preferredCurrency, companyId);
            }
            catch (DuplicatedEntryException e)
            {
                Assert.IsNotNull(e);
            }


        }


    }
}
