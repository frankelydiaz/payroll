﻿using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Payroll.Core.Contexts;
using Payroll.Core.Entities;
using Payroll.Core.Presenters;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Payroll.Specifications.Steps
{
    [Binding]
    public class ShowPaymentReportSteps : BaseSteps
    {

        private IWeeklyPaymentPresenter _paymentPresenter;

        [Given(@"The company with id (.*) and name ""(.*)"" with its base currency in ""(.*)"" with the hours paid at a fixed price of ""(.*)""")]
        public void CreateCompany(int companyId, string companyName, string baseCurrency, decimal amount)
        {
            Container.Resolve<ICreateCompanyContext>().Exec(companyId, companyName, baseCurrency, amount);
        }

        [Given(@"The currency conversion rates for ""(.*)"" are")]
        public void GivenTheCurrencyConversionRatesForAre(string baseCurrency, Table table)
        {
            var currencyRates = table.Rows.Select(s => new CurrencyRate
            {
                BaseCurrency = baseCurrency,
                Bid = decimal.Parse(s["Value"]),
                Currency = s["Currency"]
            }).ToList();

            Container.Resolve<IRegisterCurrencyRatesContext>().Exec(currencyRates);
        }

        [Given(@"the employee with id (.*) with fullname ""(.*)"" with The preferred currency ""(.*)"" in the company (.*) is registered")]
        public void CreateEmployee(int employeeId, string employeeName, string preferredCurrency, int companyId)
        {
            Container.Resolve<ICreateEmployeeContext>().Exec(employeeId, employeeName, preferredCurrency, companyId);
        }


        [When(@"Employee (.*) Reported (.*) worked hours in the week number (.*) with currency ""(.*)""")]
        public void ReportEmployeeWorkedHours(int employeeId, int workedHours, int week, string preferredCurrency)
        {
            Container.Resolve<IReportEmployeeWorkedHoursInWeekContext>().Exec(workedHours, week, employeeId, preferredCurrency);
        }

        [Then(@"I process the payment report for the company (.*) for the week number (.*) and I should see the following report:")]
        public void ThenIProcessThePaymentReportForTheCompanyForTheWeekNumberAndIShouldSeeTheFollowingReport(int companyId, int week, Table table)
        {
            _paymentPresenter = Container.Resolve<IProcessWeeklyPaymentContext>().Exec(companyId, week);

            table.CompareToSet(_paymentPresenter.Rows);
        }

        [Then(@"The total by currency should be:")]
        public void ThenTheTotalByCurrencyShouldBe(Table table)
        {
            table.CompareToSet(_paymentPresenter.TotalPaymentByCurrencies);
        }

        [Then(@"the total should be (.*)")]
        public void ThenTheTotalShouldBe(decimal total)
        {
           Assert.AreEqual(_paymentPresenter.Total,total);
        }




    }
}
